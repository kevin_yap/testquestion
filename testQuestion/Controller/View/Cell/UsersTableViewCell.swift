//
//  UsersTableViewCell.swift
//  testQuestion
//
//  Created by Kevin on 25/01/2020.
//  Copyright © 2020 kevin. All rights reserved.
//

import UIKit

class UsersTableViewCell: UITableViewCell {

    @IBOutlet fileprivate weak var imgView: UIImageView! {
        didSet {
            imgView.backgroundColor = .theme
        }
    }
    
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    
    var title: String? {
        didSet {
            
            guard let title = title else {
                return
            }
            
            titleLabel.text = title
        }
    }
    
    var img: UIImage? {
        didSet {
            
            guard let img = img else {
                return
            }
            
            imgView.image = img
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(title: String?, image: UIImage? = UIImage()) {
        
        self.title = title
        self.img = image
    }

}
