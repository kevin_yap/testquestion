//
//  UserDetailsTableViewController.swift
//  testQuestion
//
//  Created by Kevin on 26/01/2020.
//  Copyright © 2020 kevin. All rights reserved.
//

import UIKit

class UserDetailsTableViewController: UITableViewController {
    
    @IBOutlet weak var cancelBarButtonItem: UIBarButtonItem! {
        didSet {
            cancelBarButtonItem.tintColor = .theme
        }
    }
    
    @IBOutlet weak var saveBarButtonItem: UIBarButtonItem! {
        didSet {
            saveBarButtonItem.tintColor = .theme
        }
    }
    
    @IBOutlet weak var profileImgView: UIImageView! {
        didSet {
            profileImgView.backgroundColor = .theme
        }
    }
    
    @IBOutlet weak var firstNameUserDetailView: UserDetailView! {
        didSet {
            firstNameUserDetailView.textField.delegate = self
            firstNameUserDetailView.textField.returnKeyType = .next
        }
    }
    
    @IBOutlet weak var lastNameUserDetailView: UserDetailView! {
        didSet {
            lastNameUserDetailView.textField.delegate = self
            lastNameUserDetailView.textField.returnKeyType = .next
        }
    }
    
    @IBOutlet weak var emailUserDetailView: UserDetailView! {
        didSet {
            emailUserDetailView.textField.delegate = self
            emailUserDetailView.textField.returnKeyType = .next
        }
    }
    
    @IBOutlet weak var phoneUserDetailView: UserDetailView! {
        didSet {
            phoneUserDetailView.textField.delegate = self
            phoneUserDetailView.textField.returnKeyType = .done
        }
    }
    
    private var userViewModel: UserViewModel? {
        didSet {
            
            guard let userViewModel = userViewModel else {
                return
            }
            
            userViewModel.delegate = self
            
        }
    }
    
    var id: String? {
        didSet {
            
            guard let id = id else {
                return
            }
            
            let usersViewModel = UsersViewModel()
            
            if let index = usersViewModel.getIndexUserById(id) {
                
                let userModel = usersViewModel.users[index]
                userViewModel = UserViewModel(user: userModel)
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addTapDismissKeyboard()
        loadDetails()
        
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        if let header = view as? UITableViewHeaderFooterView {
            
            header.backgroundView?.backgroundColor = .header
            header.textLabel?.font = UIFont.boldSystemFont(ofSize: 22.0)
            header.textLabel?.textColor = .black
        }
    }
    
    func loadDetails() {
        
        guard let userViewModel = userViewModel else {
            return
        }
        
        let user = userViewModel.user
        
        firstNameUserDetailView.textField.text = user.firstName
        lastNameUserDetailView.textField.text = user.lastName
        emailUserDetailView.textField.text = user.email
        phoneUserDetailView.textField.text = user.phone
    }
    
    @IBAction func didTapCancelBarButton(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapSaveButton(_ sender: UIBarButtonItem) {
        
        let firstName = firstNameUserDetailView.textField.text
        let lastName = lastNameUserDetailView.textField.text
        let email = emailUserDetailView.textField.text
        let phone = phoneUserDetailView.textField.text
        
        guard let userViewModel = userViewModel  else {
            
            let userModel = UserModel(id: nil, firstName: firstName,
                                      lastName: lastName,
                                      email: email,
                                      phone: phone)
            let userViewModel = UserViewModel(user: userModel)
            self.userViewModel = userViewModel
            userViewModel.saveUser()
            
            return
        }
        
        userViewModel.user.firstName = firstName
        userViewModel.user.lastName = lastName
        userViewModel.user.email = email
        userViewModel.user.phone = phone
        
        userViewModel.saveUser()
        
    }
    
}

extension UserDetailsTableViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == firstNameUserDetailView.textField {
            
            lastNameUserDetailView.textField.becomeFirstResponder()
        }else if textField == lastNameUserDetailView.textField {
            
            emailUserDetailView.textField.becomeFirstResponder()
            
        }else if textField == emailUserDetailView.textField {
            
            phoneUserDetailView.textField.becomeFirstResponder()
        }else {
            
            textField.resignFirstResponder()
        }
        
        return true
    }
}

extension UserDetailsTableViewController: UserViewModelDelegate {
    
    func onErrorMessage(_ message: String) {
        
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(alertAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func onSuccessSave(_ message: String) {
        
        let alertController = UIAlertController(title: "Success", message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default) { (_) in
            self.navigationController?.popViewController(animated: true)
        }
        alertController.addAction(alertAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
}
