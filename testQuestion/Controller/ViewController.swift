//
//  ViewController.swift
//  testQuestion
//
//  Created by Kevin on 23/01/2020.
//  Copyright © 2020 kevin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            
            tableView.addSubview(refreshControl)
        }
    }
    
    @IBOutlet weak var addUserBarButtonItem: UIBarButtonItem! {
        didSet {
            
            addUserBarButtonItem.tintColor = .theme
        }
    }
    
    var usersViewModel: UsersViewModel = UsersViewModel() {
        didSet {
            
            self.tableView.reloadData()
        }
    }
    
    lazy var refreshControl: UIRefreshControl = {
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = .theme
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh", attributes: [
            NSAttributedString.Key.foregroundColor: UIColor.theme,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16.0)
        ])
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)

        self.usersViewModel = UsersViewModel()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "editUserSegue",
            let destination = segue.destination as? UserDetailsTableViewController,
            let row = tableView.indexPathForSelectedRow?.row {

            let user = usersViewModel.users[row]
            destination.id = user.id
            
        }
        
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            refreshControl.endRefreshing()
            self.usersViewModel = UsersViewModel()
        }
    }
    
}

extension ViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return usersViewModel.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UsersTableViewCell") as! UsersTableViewCell
        
        let user = usersViewModel.users[indexPath.row]
        
        if let firstName = user.firstName,
            let lastName = user.lastName {

            cell.configure(title: "\(firstName) \(lastName)")
        }
        
        return cell
    }
    
}

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
