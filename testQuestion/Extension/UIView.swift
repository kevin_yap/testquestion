//
//  UIView.swift
//  testQuestion
//
//  Created by Kevin on 26/01/2020.
//  Copyright © 2020 kevin. All rights reserved.
//

import UIKit

extension UIView {
    
    func addTapDismissKeyboard() {
        
        let tapDismissKeyboard = UITapGestureRecognizer(target: self, action: #selector(onTapDismissKeyboard(_:)))
        tapDismissKeyboard.numberOfTouchesRequired = 1
        tapDismissKeyboard.cancelsTouchesInView = false
        self.addGestureRecognizer(tapDismissKeyboard)
    }
    
    @objc private func onTapDismissKeyboard(_ sender: UITapGestureRecognizer){
        self.endEditing(true)
    }
    
}
