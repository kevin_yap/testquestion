//
//  Helper.swift
//  testQuestion
//
//  Created by Kevin on 25/01/2020.
//  Copyright © 2020 kevin. All rights reserved.
//

import Foundation


class Helper {
    
    static func readDataFormJsonFile(forResource resource: String, offType type: String) -> Data? {
        
        guard let path = Bundle.main.path(forResource: resource, ofType: type) else {
            return nil
        }
        
        do {
            
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            return data
            
        }catch {
            
            return nil
        }
        
    }
    
    static func writeDataToJsonFile(forResource resource: String, offType type: String, data: Data) {
        
        guard let path = Bundle.main.path(forResource: resource, ofType: type) else {
            return
        }
        
        do {
            
            try data.write(to: URL(fileURLWithPath: path))
            
        }catch {
            
            print("failed write to json file")
        }
        
    }
    
    static func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyz0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
}
