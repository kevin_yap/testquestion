//
//  User.swift
//  testQuestion
//
//  Created by Kevin on 24/01/2020.
//  Copyright © 2020 kevin. All rights reserved.
//

import Foundation


struct UserModel: Codable {
    
    var id: String?
    var firstName: String?
    var lastName: String?
    var email: String?
    var phone: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case firstName
        case lastName
        case email
        case phone
    }
    
}

extension UserModel {
    
    init(data: Data) throws {
        self = try JSONDecoder().decode(UserModel.self, from: data)
    }
    
}
