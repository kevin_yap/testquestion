//
//  UserViewModel.swift
//  testQuestion
//
//  Created by Kevin on 24/01/2020.
//  Copyright © 2020 kevin. All rights reserved.
//

import Foundation
import UIKit

@objc protocol UserViewModelDelegate {
    func onErrorMessage(_ message: String)
    @objc optional func onSuccessSave(_ message: String)
}

class UserViewModel {
    
    var user: UserModel
    var delegate: UserViewModelDelegate?
    
    init(user: UserModel) {
        
        self.user = user
    }
    
    func saveUser() {
        
        if validator() {
            let usersViewModel = UsersViewModel()
            usersViewModel.delegate = self
            usersViewModel.addUser(user)
        }
    }
    
    private func validator() -> Bool {
        
        if user.firstName == "" {
            delegate?.onErrorMessage("The first name field is required.")
            return false
        }else if user.lastName == "" {
            delegate?.onErrorMessage("The last name field is required.")
            return false
        }else if let email = user.email,
            email != "",
            !email.isValidEmailAddress() {
            
            delegate?.onErrorMessage("The email must be a valid email address.")
            return false
        }else if let phone = user.phone,
            phone != "",
            !phone.isValidPhone {
            
            delegate?.onErrorMessage("The phone must be a valid phone number.")
        }
        
        
        
        return true
        
    }
    
}

extension UserViewModel: UsersViewModelDelegate {
    
    func onFailedWrite(_ message: String) {
        delegate?.onErrorMessage(message)
    }
    
    func onSuccessWrite(_ message: String) {
        delegate?.onSuccessSave?(message)
    }
    
}
