//
//  UsersViewModel.swift
//  testQuestion
//
//  Created by Kevin on 26/01/2020.
//  Copyright © 2020 kevin. All rights reserved.
//

import Foundation
import UIKit

protocol UsersViewModelDelegate {
    func onFailedWrite(_ message: String)
    func onSuccessWrite(_ message: String)
}

class UsersViewModel {
    
    var users: [UserModel]
    var delegate: UsersViewModelDelegate?
    
    init() {
        self.users = self.loadData
    }
    
    public func addUser(_ user: UserModel) {
        
        if let id = user.id,
           let row = getIndexUserById(id) {
            
            users[row] = user
        }else {
            
            var userModel = user
            
            let id = getRandomId()
            userModel.id = id
            users.insert(userModel, at: 0)
        }
        
        writeData()
    }
    
    public func getIndexUserById(_ id: String) -> Int? {
        return users.firstIndex{ $0.id == id }
    }
    
    public func writeData() {
        
        do {
            
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let data = try encoder.encode(self.users)
            
            Helper.writeDataToJsonFile(forResource: "data", offType: "json", data: data)
            delegate?.onSuccessWrite("success write to json file")
            
        }catch {
            delegate?.onFailedWrite("failed write to json file")
        }
        
    }
    
    private func getRandomId() -> String {
        
        let number = "5c8a80f5\(Helper.randomString(length: 16))"
        
        if let _ = getIndexUserById(number) {
            return getRandomId()
        }else {
            return number
        }
        
    }
    
    private var loadData: [UserModel] = { () -> [UserModel] in
        
        guard let data = Helper.readDataFormJsonFile(forResource: "data", offType: "json") else {
            return []
        }
        
        do {
            let results = try JSONDecoder().decode([UserModel].self, from: data)
            
            return results
        }catch {
            return []
        }
        
    }()
    
}
