//
//  UserDetailView.swift
//  testQuestion
//
//  Created by Kevin on 26/01/2020.
//  Copyright © 2020 kevin. All rights reserved.
//

import UIKit

@IBDesignable
class UserDetailView: UIView {
    
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    @IBInspectable var title: String? {
        didSet {
            
            guard let title = title else {
                return
            }
            
            titleLabel.text = title
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadViewFromNib()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadViewFromNib()
    }
    
    private func loadViewFromNib() {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "UserDetailView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.addSubview(view)
    }
    
}
